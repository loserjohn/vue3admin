/*
 * @Author: your name
 * @Date: 2022-02-17 22:55:36
 * @LastEditTime: 2022-03-01 23:48:09
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3admin\src\layout\components\index.js
 */
export { default as AppMain } from './AppMain'
export { default as Navbar } from './Navbar'
// export { default as Settings } from './Settings'
export { default as Sidebar } from './Sidebar/index.vue'
// export { default as TagsView } from './TagsView/index.vue'
