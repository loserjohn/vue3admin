/*
 * @Author: loserjohn
 * @Date: 2022-03-20 11:28:24
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-03-20 15:12:54
 * @Description: ---
 * @FilePath: \vue3admin\src\i18n\index.js
 */
// import storage from '@/utils/storage'
import enLocale from 'element-plus/lib/locale/lang/en'
import zhLocale from 'element-plus/lib/locale/lang/zh-cn'
import Cookies from 'js-cookie'
import { createI18n } from 'vue-i18n'
import en from './locales/en'
import zh from './locales/zh-cn'
// const i18n = createI18n({
//   locale: 'zh', // set locale
//   messages: {
//     zh: {
//       message: {
//         hello: '你好世界'
//       }
//     },
//     en: {
//       message: {
//         hello: 'hello world'
//       }
//     }
//   }
// })
console.log(enLocale, zhLocale)
const messages = {
  [enLocale.name]: {
    el: enLocale.el,
    ...en
  },
  [zhLocale.name]: {
    el: zhLocale.el,
    ...zh
  }
}

export const getLocale = () => {
  const cookieLanguage = Cookies.get('language', language)
  if (cookieLanguage) {
    return cookieLanguage
  }
  const language = navigator.language.toLowerCase()
  const locales = Object.keys(messages)
  for (const locale of locales) {
    if (language.indexOf(locale) > -1) {
      return locale
    }
  }

  // Default language is zh-cn
  return zhLocale.name
}
console.log('default', getLocale())
const i18n = createI18n({
  locale: getLocale(),
  messages: messages
})

export default i18n
