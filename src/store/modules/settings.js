/*
 * @Author: your name
 * @Date: 2022-02-27 12:03:37
 * @LastEditTime: 2022-03-20 17:03:52
 * @LastEditors: loserjohn
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3admin\src\store\modules\settings.js
 */
import defaultSettings from '@/settings'
import variables from '@/styles/variables.scss'

const { showSettings, tagsView, fixedHeader, sidebarLogo } = defaultSettings

const state = {
  theme: variables.theme,
  showSettings: showSettings,
  tagsView: tagsView,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    // if (state.hasOwnProperty(key)) {
    //   state[key] = value
    // }
  }
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_SETTING', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

