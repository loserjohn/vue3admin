import Layout from '@/layout'
import {
  constantRoutes,
  routeComponent
} from '@/router'
// import { off } from 'codemirror'

// console.log('routeComponent',routeComponent)
/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  },
  RESET_ROUTES: (state, routes) => {
    state.addRoutes = []
    state.routes = []
  }
}

const actions = {
  generateRoutes({
    commit
  }, menus) {
    return new Promise(resolve => {
      console.log('menus', menus)
      const accessedRoutes = filterAsyncRoutes(menus)

      accessedRoutes.push({
        path: '/:pathMatch(.*)*',
        name: 'Not',
        hidden: true,
        component: Layout,
        children: [
          {
            path: '',
            component: () => import('@/views/error-page/404'),
            name: 'NotFound',
            meta: {
              title: '页面找不到拉',
              icon: 'NotFound',
              affix: true
            }
          }

        ]

      })

      // console.log('accessedRoutes', accessedRoutes)

      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  },
  reset({
    commit
  }) {
    commit('RESET_ROUTES')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

export function filterAsyncRoutes(menus) {
  const res = []
  menus.map(it => {
    // debugger
    const metas = {}
    metas['id'] = it.id
    metas['title'] = it.display_name
    metas.icon = it.icon
    const item = {
      path: `/${it.name}`,
      name: it.name,
      redirect: '',
      meta: metas,
      hidden: it.status != '1' || it.is_menu != '1'
    }

    // item.component = () => Promise.resolve(require(`@/views${it.page_url.split('/:')[0]}`).default)
    item.component = Layout
    if (it.subs && it.subs.length > 0) {
      item.children = filterSon(it.subs)
    }

    res.push(item)
  })
  return res
}

export function filterSon(menus) {
  const res = []
  menus.map(it => {
    const metas = {}

    metas['id'] = it.id
    metas['title'] = it.display_name
    metas.icon = it.icon
    const item = {
      // path: it.page_url,
      name: it.name,
      redirect: '',
      // meta: metas,
      hidden: it.status != '1' || it.is_menu != '1'
    }
    if (routeComponent[it.name]) {
      // debugger
      const {
        path
      } = routeComponent[it.name]
      // debugger
      const urls = path.split('?')
      if (urls.length > 1 && urls[1].length > 0) {
        const params_items = urls[1].split('&')
        for (let i = 0; i < params_items.length; i++) {
          const items = params_items[i].split('=')
          metas[items[0]] = items[1]
        }
      }

      // item.component = () => Promise.resolve(require(`@/views${it.page_url.split('/:')[0]}`).default)
      item.component = routeComponent[it.name].component
      // debugger
      item.path = path
      item.meta = metas
    } else {
      item.path = it.name
      item.meta = metas
      item.redirect = '404'
      item.component = () => import('@/views/error-page/404')
    }
    if (it.subs && it.subs.length > 0) {
      item.children = filterSon(it.subs)
    }

    res.push(item)
  })
  return res
}
