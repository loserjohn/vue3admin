import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/login',
    method: 'post',
    data
  })
}

export function getInfo(site) {
  return request({
    url: '/api/GetInfo',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/LoginOut',
    method: 'post'
  })
}
