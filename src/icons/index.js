/*
 * @Author: your name
 * @Date: 2022-02-17 22:55:34
 * @LastEditTime: 2022-02-18 22:22:20
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3admin\src\icons\index.js
 */
import SvgIcon from '@/components/SvgIcon' // svg组件
import { createApp } from 'vue'
// import { Vue } from 'vue'

// console.log('Vue', createApp())
createApp().component('svg-icon', SvgIcon)

const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)

