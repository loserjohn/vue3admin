/*
 * @Descripttion:
 * @version:
 * @ Modified by: loserjohn
 * @ Modified time: 2022-02-26 22:04:17
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-02-26 22:04:17
 */
import store from '@/store'
import {
  getToken
} from '@/utils/auth'
import axios from 'axios'
import { ElMessage } from 'element-plus'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: false, // send cookies when cross-domain requests
  timeout: 30000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    if (store.getters.token) {
      // let each request carry token --['X-Token'] as a custom key.
      // please modify it according to the actual situation.
      config.headers['Authorization'] = getToken()
    }

    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code.
   */
  response => {
    const res = response.data
    // debugger

    if (response.status >= 200 && response.status < 300) {
      // ElMessage({
      //   message: 'yes',
      //   type: 'success',
      //   duration: 5 * 1000
      // })
      return res
    } else {
      ElMessage({
        message: JSON.stringify(res),
        type: 'error',
        duration: 5 * 1000
      })
      // return res
      return Promise.reject(res)
    }
    // if (res.status ===  401) {
    //   // to re-login
    //   MessageBox.confirm('身份过期，请重新登录。', '确认退出', {
    //     confirmButtonText: '重新登陆',
    //     cancelButtonText: '取消',
    //     type: 'warning'
    //   }).then(() => {
    //     store.dispatch('user/resetToken').then(() => {
    //       location.reload()
    //     })
    //   })
    // }else{

    // }
  },
  (error) => {
    // debugger
    console.log('err：', error.response) // for debug
    const err = error.response.data || null
    // Message({
    //   message: err && err.message ? err.message : err.request_url,
    //   type: 'error',
    //   duration: 5 * 1000
    // })
    // if (error.message.indexOf('401') >= 0) {
    //   router.push({
    //     path: '/401'
    //   })
    // }

    return Promise.reject(err)
  }
)

export default service
