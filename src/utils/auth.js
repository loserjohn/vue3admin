/*
 * @Author: your name
 * @Date: 2022-02-26 12:35:51
 * @LastEditTime: 2022-03-20 14:48:44
 * @LastEditors: loserjohn
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3admin\src\utils\auth.js
 */
import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const MenusKey = 'Admin-Menus'
const UserKey = 'Admin-UserInfo'

export function getToken() {
  const c = Cookies.get(TokenKey)
  return c ? `Bearer ${c}` : null
}

export function setToken(token) {
  // debugger
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  // return Cookies.remove(TokenKey)
}

export function getMenus() {
  const str = localStorage.getItem(MenusKey)
  return str ? JSON.parse(str) : null
}

export function setMenus(m) {
  if (!m) return
  const str = JSON.stringify(m)
  return localStorage.setItem(MenusKey, str)
}

export function removeMenus() {
  return localStorage.removeItem(MenusKey)
}

export function getUserInfo() {
  const str = localStorage.getItem(UserKey)
  return str ? JSON.parse(str) : null
}

export function setUserInfo(m) {
  if (!m) return
  const str = JSON.stringify(m)
  return localStorage.setItem(UserKey, str)
}
