/*
 * @Descripttion:
 * @version:
 * @Author: loserjohn
 * @Date: 2020-09-27 14:35:22
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-03-08 22:48:00
 */
import {
  getMenus,
  getToken
} from '@/utils/auth' // get token from cookie
import {
  ElMessage
} from 'element-plus'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import router, {
  constantRoutes
} from './router'
import store from './store'
// import getPageTitle from '@/utils/get-page-title'

let a = false
NProgress.configure({
  showSpinner: false
}) // NProgress Configuration

const whiteList = ['/login', '/auth-redirect'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = to.meta.title
  // debugger
  // determine whether the user has logged in
  const hasToken = getToken()

  // console.log('to', to)
  const menus = getMenus()
  // debugger
  if (hasToken && menus) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({
        path: '/'
      })
      NProgress.done()
    } else {
      // determine whether the user has obtained his permission roles through getInfo
      const hasUser = store.getters.permission_routes && store.getters.permission_routes.length > 0

      // debugger
      if (hasUser) {
        // //debugger
        next()
      } else {
        // debugger
        try {
          const accessRoutes = await store.dispatch('permission/generateRoutes', menus)
          // console.log('accessRoutes', accessRoutes)
          // dynamically add accessible routes
          // router.resetRouter()
          const integralRoutes = constantRoutes.concat(accessRoutes)
          // const integralRoutes = constantRoutes
          console.log('integralRoutes', integralRoutes)
          // router.options.routes = integralRoutes
          // router.addRoutes(integralRoutes)
          integralRoutes.map(it => {
            // console.log('it', it)
            if (it.path && it.path.startsWith('/')) {
              router.addRoute(it)
            }
          })
          console.log('router', router.getRoutes())
          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          a = true
          next({
            ...to,
            replace: true
          })
        } catch (error) {
          console.log('permissionerror', error)
          // debugger
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          ElMessage.error(JSON.stringify(error) || '路由问题')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/
    // debugger
    if (whiteList.includes(to.path)) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
