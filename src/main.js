/*
 * @Author: your name
 * @Date: 2022-02-17 22:17:06
 * @LastEditTime: 2022-03-20 18:18:45
 * @LastEditors: loserjohn
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3admin\src\main.js
 */
import svgIcon from '@/components/SvgIcon/index.vue'
import '@/icons'
import '@/permission.js'
import '@/styles/index.scss'
import '@/styles/variables.scss'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import { createApp } from 'vue'
import App from './App.vue'
import i18n from './i18n'
import router from './router'
import store from './store'

const app = createApp(App)
// 设置全局
app.component(
  'svg-icon',
  svgIcon
)
app.use(ElementPlus, { size: 'mini', zIndex: 3000 }).use(i18n).use(store).use(router).mount('#app')
