/*
 * @Author: your name
 * @Date: 2022-02-17 22:17:06
 * @LastEditTime: 2022-02-26 21:31:03
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3admin\babel.config.js
 */
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
    [
      '@babel/preset-env',
      {
        'modules': 'auto'
      }
    ]
  ]
}
